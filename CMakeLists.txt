cmake_minimum_required(VERSION 3.12)
project(wx_opencv_helloworld)

find_package( OpenCV REQUIRED )
include_directories( ${OpenCV_INCLUDE_DIRS} )
# set(CMAKE_CXX_STANDARD 20)
# set(COMPILE_DEFINITIONS -02 -Wall)

find_package( wxWidgets REQUIRED net core base )
include( ${wxWidgets_USE_FILE} )

add_executable( wx_opencv_helloworld src/wx_opencv_helloworld.cpp )
target_link_libraries( wx_opencv_helloworld ${OpenCV_LIBS} )
target_link_libraries( wx_opencv_helloworld ${wxWidgets_LIBRARIES} )