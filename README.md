# Crosscompile + Remote Debug 
> I have only tested this from my development computer: ubuntu, 22.04.3 LTS (Jammy Jellyfish)

This was my journey to set up cross compilation and remote debugging with vscode between my host machine (x86_64, ubuntu desktop) and a rpi4 (aarch64, raspbian desktop). The following guide is the culmination of several helpful and unhelpful online guides until I finally got everything working.

Currently only setup for opencv + wxWidgets.

![She runs](docu_imgs/example.png "She's Alive!")

### Rpi4 setup
> The following instructions are to be run on your target (the raspberry pi, aarch64)

I wanted to install wxWidgets with the package manager and not from source, I saw you were using 3.1/3.2.

My pi's sources pointed to bullseye, which as of early 2023 bullseye is `oldstable` and includes wxWidgets v3.0.5.1, so upgraded my sources to current stable: bookworm 
> There should be no significant deviation from this guide if you plan to build+install wxWidgets from source

This upgrade take ~15 mins, we are upgrading the entire system (almost every package)
```sh
sudo apt-get update && sudo apt-get dist-upgrade
sudo sed -i -e 's/bullseye/bookworm/g' /etc/apt/sources.list
sudo sed -i -e 's/bullseye/bookworm/g' /etc/apt/sources.list.d/raspi.list
sudo apt update
sudo apt -y full-upgrade
sudo apt -y autoremove
sudo apt -y clean
sudo apt purge ?config-files
sudo reboot
```

#### Install wxWidgets
```sh
sudo apt install -y libwxgtk3.2-dev  
```

Which installed additional libraries aswell:
```
The following NEW packages will be installed:
  libgl-dev libglu1-mesa libglu1-mesa-dev libglx-dev libopengl-dev libwxbase3.2-1 libwxgtk-gl3.2-1 libwxgtk3.2-1 libwxgtk3.2-dev wx-common wx3.2-headers
0 upgraded, 11 newly installed, 0 to remove and 0 not upgraded.
```

***wx allows you to have multiple versions installed*** 
> EDIT: I think this actually handled when you use the output from: `wx-config --cxxflags` & `wx-config --libs` commands

This is one of the errors we were running into, to "pick" a version that we want to use we can create a symbolic link "wx" to the "wx-3.2/wx" directory 
```sh
cd /usr/include
sudo ln -s wx-3.2/wx wx
```

### Host set up
> these command are run on the host, the development computer (ubuntu, x86_64)

#### Required installs
```sh
# cross compilers
sudo apt install gcc-aarch64-linux-gnu g++-aarch64-linux-gnu

# multiarch debugger
sudo apt install gdb-multiarch

# rsync to create sysroot
sudo apt install rsync
```

#### ssh setup (required) 
```sh
# I specified the type here, I think ed25519 is one of the strongest (not important, because I am not using a passphrase)
ssh-keygen -t ed25519 
# copy your public key to the remote machine (pi), this will add the public key to the .ssh/authorized_keys on the pi
ssh-copy-id -i ~/.ssh/id_ed25519_doom <pi-username>@<pi-ip-address>
# config ssh for vscode
touch ~/.ssh/config # create config, if a ssh config doesn't already exist
# change "ian" & "doom" accordingly (if you haven't set the hostname of the pi, I think you can skip it?)
# vscode's remote extension can also create this file, I think
cat > ~/.ssh/config << EOF 
Host <pis-ip-address/hostname>
  HostName doom
  User ian
  ForwardX11 yes
EOF
# now you can login, password free, with:
ssh ian@doom
```

#### create sysroot
```sh
# this command did give me a couple errors, I never investigated them. rsync has ALOT of flags, could probably invesitgate further to prevent errors 
rsync -vrl --delete-after --safe-links ian@doom:/{lib,usr} $HOME/rpi4-doom-sysroot/

# wx-config wasn't copied for some reason, so I manually copied it, check first with: `find rpi4-doom-sysroot -name "wx-config"`
cd ~/rpi-doom-sysroot/usr/bin
scp ian@doom:/usr/bin/wx-config .
```

#### vscode config & toolchain files to edit
##### Install these extensions
- C/C++ (microsoft)
- CMake Tools (Microsoft)
- And vim (if your a mad man)

![My enabled extensions](docu_imgs/extensions.png "My enabled extensions")

##### You will have to edit these files:
toolchain.cmake:
- line 9: change ~~/home/ian/rpi4-doom-sysroot~~ -> Path to where you copied your sysroot
- .vscode/launch.json
  - change ~~/home/ian/rpi4-doom-sysroot~~ -> Path to where you copied your sysroot 
  - change ~~ian@doom~~ to your <pi-username>@<ip-address/hostname>
- .vscode/tasks.json
  - change ~~ian@doom~~ to your <pi-username>@<ip-address/hostname>
- c_cpp_properties.json
  - itellisense looks for the source files at the paths listed here
  - change ~~/home/ian/rpi4-doom-sysroot~~ -> Path to where you copied your sysroot 

### TODOs
- add task that builds project before starting debugger
- add build option for host build (eg. without toolchain, using compiler and sources for host machine)
- test `toolchain.cmake`` for the truely required library and include folders
